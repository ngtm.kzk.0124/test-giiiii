# -*- coding: utf-8 -*-

import discord # discord.py のインポート
import datetime as dt#　datetimeライブラリのインポート
import json

#from discord.ext import tasks
#from sp_file import gspread_simle_ as gsp 
from sp_file import gss_test as gsp_ano #中村くんからもらったファイル
#import requests
#現在の時間の取得
now = dt.datetime.now()
print("現在時刻：", now)
before_login = dt.datetime(2020, 4, 16, 12, 30, 20, 1230)

# Botのアクセストークン
TOKEN = 'ODMyMTM2NDczNTU2OTQyOTEx.YHfZew.P_T_Q-KJs-yhgWXJKAoCddLFNlM'

# DiscordのサーバURL
MY_SERVER = '832437550072987688' # "botよう"
#MY_SERVER = '832094274387574804' # "WHY-NN"
#MY_SERVER = '832124674954625054' # "ながともちゃんのサーバ"

# Slackのweb hook URL
WEB_HOOK_URL = 'https://hooks.slack.com/services/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'

# 接続に必要なオブジェクトを生成
intents = discord.Intents.all()
intents.members = True
client = discord.Client(intents=intents)

# 列を変更するためのindex(初期値：H列)
index = 8

# コマンドに対応するリストデータを取得する関数を定義
def get_data(message):
    #print(message)
    command = message.content
    data_table = {
        '/members': message.guild.members, # メンバーのリスト
        '/roles': message.guild.roles, # 役職のリスト
        '/text_channels': message.guild.text_channels, # テキストチャンネルのリスト
        '/voice_channels': message.guild.voice_channels, # ボイスチャンネルのリスト
        '/category_channels': message.guild.categories, # カテゴリチャンネルのリスト
        '/members_num' : len(message.guild.members),
    }
    return data_table.get(command, '無効なコマンドです')


""""""""""""""""""""""イベントハンドラ"""""""""""""""""""""""""""

# botのログイン処理
@client.event
async def on_ready():
    print(client.user, 'がログインしました')
    print("最終ログイン（仮）：", before_login)
    print("ログインしなかった日数：", now - before_login, '\n')

# 発言時に実行されるイベントハンドラを定義
@client.event
async def on_message(message):
    # コマンドに対応するデータを取得して表示    
    if message.content == '/test':
        # message インスタンスから guild インスタンスを取得
        guild = message.guild 
        # ユーザのみ
        user_count = sum(1 for member in guild.members if not member.bot)
        msg = f'ユーザ数：{user_count}'
        # gss_testから replay_msg関数の呼び出し：引数 message, client, msg(送信したい内容)
        gsp_ano.replay_msg(message, client, msg)

    elif message.content == '/read':
        msg_from_gsp = gsp_ano.get_spread_data()    
        print(msg_from_gsp)

# ボイスチャット開始時
@client.event
async def on_vc_start(member,channel):
    print("入室者　　　:", member.name)
    print("チャンネル名:", channel, '\n')

# ボイスチャット終了時
@client.event
async def on_vc_end(member,channel):   
    global index
    index += 1
    if index > 12:
        index = 8 #

    print("退出者　　　:", member.name)
    print("チャンネル名:", channel, '\n')

# 新しいメンバーがきた際にSlackに通知
@client.event
async def on_member_join(member):
    await member.send('prinvate')
    print("join")

# Discordのボイスチャットの状態を取得
@client.event
async def on_voice_state_update(member,before,after):
    if before.channel != after.channel:
        # ボイスチャットが開始
        # before.channelとafter.channelが異なるなら入退室
        if after.channel and len(after.channel.members) == 1:
            start_time = dt.datetime.now()
            print(f"開始時刻　　:{start_time:%Y/%m/%d-%H:%M}")
            client.dispatch("vc_start",member,after.channel) #発火！
            #time_log = gsp.Time_Log()
            #time_log.before_time = start_time

        # ボイスチャットが終了
        if before.channel and len(before.channel.members) == 0:
            end_time = dt.datetime.now()
            print("終了時刻　　:{end_time:%Y/%m/%d-%H:%M}")
            end_time_char = f'{end_time:%Y/%m/%d-%H:%M}'
            print(type(end_time))
            int_end_time = end_time.year
            print(type(int_end_time))
            # もし、ボイスチャットが終了したら
            client.dispatch("vc_end",member,before.channel) #発火！

            #ボスチャットの終了時刻をスプレッドシートに格納
            gsp_ano.replay_log(client, end_time_char, before, index)

          


# Botの起動とDiscordサーバーへの接続
client.run(TOKEN)
